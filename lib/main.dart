import 'package:demo/database.dart';
import 'package:demo/photo_model.dart';
import 'package:flutter/material.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

import 'app.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  AppDatabase database = AppDatabase();
  await database.createDB();

  runApp(MyApp());
}
