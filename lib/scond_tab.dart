import 'package:demo/colors_model.dart';
import 'package:demo/photo_model.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SecondTab extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    if (Provider.of<PhotosModel>(context, listen: false).photos.isEmpty)
      Provider.of<PhotosModel>(context, listen: false).getPhotos();

    return Consumer<PhotosModel>(
      builder: (context, model, child) {
        return ListView.builder(
          itemCount: model.photos.length,
          itemBuilder: (_, index) {
            Photo photo = model.photos[index];
            return Container(
              height: 100,
              margin: EdgeInsets.all(2),
              color: Colors.amber,
              child: Image.network(
                photo.url,
                fit: BoxFit.cover,
              ),
            );
          },
        );
      },
    );
  }
}
