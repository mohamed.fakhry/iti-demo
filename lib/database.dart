import 'package:demo/photo_model.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:sqflite/sqlite_api.dart';

class AppDatabase {
  static final AppDatabase instance = AppDatabase._internal();

  Database database;

  factory AppDatabase() {
    return instance;
  }

  AppDatabase._internal() {
    print("_internal");
    createDB();
  }

  createDB() async {
    database = await openDatabase(
      join(await getDatabasesPath(), 'photos.db'),
      onCreate: (db, version) {
        print("oncreate");
        db.execute(
          'CREATE TABLE photos(id INTEGER PRIMARY KEY, albumId INTEGER, title TEXT, url TEXT)',
        );
      },
      version: 2,
    );
  }

  insertPhoto(Photo photo) async {
    return await database.insert(
      "photos",
      photo.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future<List<Photo>> getPhotos() async {
    List<Map<String, dynamic>> maps = await database.query("photos");

    return List.generate(maps.length, (index) {
      return Photo.fromMap(maps[index]);
    });
  }
  /*
  
  Photo photo = Photo(id: 1, url: "", albumId: 2, title: "");
  print(
  
  );

  print();
  */
}
