import 'package:demo/colors_model.dart';
import 'package:demo/photo_model.dart';
import 'package:demo/scond_tab.dart';
import 'package:demo/second_page.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:provider/provider.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;
  TextEditingController emaiController;
  String email;
  List<Photo> photos = [];

  @override
  void initState() {
    emaiController = TextEditingController(text: "Default");
    // controller.text;
    // getPhotos();
    super.initState();
  }

  void _incrementCounter() {
    print("increment");
    print(emaiController.text);
    setState(() {
      // This call to setState tells the Flutter framework that something has
      // changed in this State, which causes it to rerun the build method below
      // so that the display can reflect the updated values. If we changed
      // _counter without calling setState(), then the build method would not be
      // called again, and so nothing would appear to happen.
      _counter++;
    });
  }

  Future<List<Photo>> getPhotos() async {
    // print("getPhotos Called");
    // // throw Exception("No internet connection");
    // var response = await Dio().get(
    //   'https://jsonplaceholder.typicode.com/photos',
    // );
    // for (var item in response.data) {
    //   Photo photo = Photo.fromMap(item);
    //   photos.add(photo);
    //   Provider.of<PhotosModel>(context, listen: false).addPhoto(photo);
    // }

    // return photos;
  }

  @override
  Widget build(BuildContext context) {
    print("Rebuild");
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
          bottom: TabBar(
            tabs: [
              Center(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(Icons.undo),
                      SizedBox(width: 5),
                      Text("First Tab"),
                    ],
                  ),
                ),
              ),
              Center(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(Icons.undo),
                      SizedBox(width: 5),
                      Text("First Tab"),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
        body: TabBarView(children: [
          Container(
            child: FutureBuilder<Object>(
                future: getPhotos(),
                builder: (context, snapshot) {
                  print("Rebuild in Future builder");
                  print("connectionState = " +
                      snapshot.connectionState.toString());
                  if (snapshot.connectionState == ConnectionState.waiting)
                    return Center(child: CircularProgressIndicator());

                  if (snapshot.error != null)
                    return Center(
                      child: Text(
                        (snapshot.error as Exception).toString(),
                      ),
                    );

                  return GridView.builder(
                    gridDelegate:
                        const SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2,
                    ),
                    itemCount: photos.length,
                    itemBuilder: (BuildContext context, int index) {
                      Photo photo = photos[index];

                      return GestureDetector(
                        onTap: () {
                          // Navigator.push(
                          //     context,
                          //     MaterialPageRoute(
                          //         builder: (_) => SecondPage(
                          //               photo: photo,
                          //             )));
                          showDialog(
                              context: context,
                              builder: (context) => SimpleDialog(
                                    title: Text("Logout"),
                                    // content: Text("Are you sure u want to logout?"),
                                    children: [
                                      FlatButton(
                                        onPressed: () {
                                          Navigator.pop(context);
                                        },
                                        child: Text("Yes"),
                                        color: Colors.red,
                                      ),
                                      FlatButton(
                                        onPressed: () {},
                                        child: Text("Cancel"),
                                        color: Colors.green,
                                      ),
                                    ],
                                  ));
                        },
                        child: Card(
                          color: Colors.amber,
                          child: Hero(
                            tag: photo.url,
                            child: Image.network(photo.url),
                          ),
                        ),
                      );
                    },
                  );
                }),
          ),
          SecondTab(),
        ]),
        floatingActionButton: FloatingActionButton(
          onPressed: _incrementCounter,
          tooltip: 'Increment',
          child: Icon(Icons.add),
        ), // This trailing comma makes auto-formatting nicer for build methods.
      ),
    );
  }
}
