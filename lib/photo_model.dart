class Photo {
  int id;
  int albumId;
  String title;
  String url;

  Photo({
    this.id,
    this.albumId,
    this.title,
    this.url,
  });

  Photo.fromMap(Map<String, dynamic> map) {
    id = map["id"];
    albumId = map["albumId"];
    title = map["title"];
    url = map["url"];
  }

  Map<String, dynamic> toMap() => {
        "id": id,
        "albumId": albumId,
        "title": title,
        "url": url,
      };

  @override
  String toString() {
    return id.toString();
  }
}
