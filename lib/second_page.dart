import 'package:demo/photo_model.dart';
import 'package:flutter/material.dart';

class SecondPage extends StatelessWidget {
  Photo photo;
  SecondPage({Key key, this.photo}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(photo.title ?? ""),
      ),
      body: Container(
        child: Center(
          child: GestureDetector(
            onTap: () {
              Navigator.pop(context, "Yes");
            },
            child: Hero(
              tag: photo.url,
              child: Image.network(photo.url),
            ),
          ),
        ),
      ),
    );
  }
}
