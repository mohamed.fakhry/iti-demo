import 'package:demo/photo_model.dart';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';

import 'database.dart';

class PhotosModel extends ChangeNotifier {
  List<Photo> _photos = [];

  AppDatabase _database = AppDatabase();

  PhotosModel() {
    print("PhotosModel");
    _database.getPhotos().then((photos) {
      print(photos);
      _photos = photos;
      notifyListeners();
    });
  }

  List<Photo> get photos => _photos;

  addPhoto(Photo photo) {
    _photos.add(photo);
    notifyListeners();
  }

  getPhotos() async {
    print("getPhotos Called");
    // throw Exception("No internet connection");
    var response = await Dio().get(
      'https://jsonplaceholder.typicode.com/photos',
    );
    for (var item in response.data) {
      Photo photo = Photo.fromMap(item);
      _photos.add(photo);

      _database.insertPhoto(photo);
    }

    notifyListeners();
  }
}
