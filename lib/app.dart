import 'package:demo/colors_model.dart';
import 'package:demo/home_page.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => PhotosModel(),
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          // This is the theme of your application.
          //
          // Try running your application with "flutter run". You'll see the
          // application has a blue toolbar. Then, without quitting the app, try
          // changing the primarySwatch below to Colors.green and then invoke
          // "hot reload" (press "r" in the console where you ran "flutter run",
          // or simply save your changes to "hot reload" in a Flutter IDE).
          // Notice that the counter didn't reset back to zero; the application
          // is not restarted.
          primarySwatch: Colors.blue,
          // This makes the visual density adapt to the platform that you run
          // the app on. For desktop platforms, the controls will be smaller and
          // closer together (more dense) than on mobile platforms.
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        // routes: {
        //   '/': (context) => MyHomePage(title: 'Flutter Demo Page'),
        //   '/page': (context) => SecondPage(),
        // },
        onGenerateRoute: (RouteSettings routeSettings) {
          print("==========Route==============");
          print(routeSettings.name);
          print(routeSettings.arguments);
          print("========================");

          switch (routeSettings.name) {
            case '/':
              print("We are going to Home page");
              return MaterialPageRoute(
                builder: (_) => MyHomePage(
                  title: "Home Page",
                ),
              );

            case '/page':
              print("We are going to Second page");

              String title = routeSettings.arguments;
              // if (title == "Second") {
              //   return MaterialPageRoute(
              //     builder: (_) => SecondPage(
              //       title: title,
              //     ),
              //   );
              // } else {
              //   return MaterialPageRoute(
              //     builder: (_) => MyHomePage(
              //       title: title,
              //     ),
              //   );
              // }
              break;
            default:
              return MaterialPageRoute(
                builder: (_) => Scaffold(
                  body: Container(
                    child: Text("404"),
                  ),
                ),
              );
          }
        },
        // home: MyHomePage(title: 'Flutter Demo Page'),
      ),
    );
  }
}
